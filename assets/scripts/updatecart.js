// we'll capture all inputs
// we'll attach event listeners to each input
// third, if we remove focus from the input field, it should submit the form

let quantityInputs = document.querySelectorAll(".quantityInput");

quantityInputs.forEach(editInput =>{
	editInput.addEventListener("change", () => {
		editInput.parentElement.submit();
	})
})
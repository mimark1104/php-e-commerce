// we need to get all buttons.
// we need to add an event listener to each of the btn
// get the data from the button (which is the item_id)
// get the data from the input (which is the item_quantity)
// check if quantity is < 0, reject
// if > 0, send the data via fetch


let addtocart_buttons = document.querySelectorAll(".addToCartBtn");

addtocart_buttons.forEach(function(addToCartBtn){
	addToCartBtn.addEventListener("click" , function(indiv_button){
		let id = indiv_button.target.getAttribute("data-id");
			
		let quantity = indiv_button.target.previousElementSibling.value;

		if(quantity < 0){
			alert("Please enter quantity");
		}else{
			let data = new FormData;

			data.append("id" , id);
			data.append("quantity", quantity);

			fetch("../../controllers/process_update_cart.php",
				{
				method: "POST",
				body: data
			}).then(response => {
				return response.text();
			}).then(data_from_fetch => {
				console.log(data_from_fetch);
				document.querySelector("#cartCount").innerHTML = data_from_fetch;

			})
		}
	
	})		
})
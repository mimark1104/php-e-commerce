<?php 
	require "../templates/template.php";
	function get_content(){


	require "../controllers/connection.php";
	?>
		<h1 class="text-center py-5">CATALOG</h1>

		<div class="container">
			<div class="row">
				<!-- sidebar -->
				<div class="col-lg-2">
					<h3>Categories</h3>
					<ul class="list-group border">
						<li class="list-group-item">
							<a href="catalog.php">ALL</a>
						</li>
						<?php 
							$categories_query = "SELECT * FROM categories";
							$categories = mysqli_query($conn, $categories_query);
							foreach ($categories as $indiv_categories){
								?>
									<li class="list-group-item">
										<a href="catalog.php?category_id=<?php echo $indiv_categories['id']?>"><?php echo $indiv_categories['name']?></a>
									</li>
								<?php
							}
						 ?>
					</ul>
					<!-- sorting -->
					<h3>Sort By</h3>
					<ul class="list-group border">
						<li class="list-group-item">
							<a href="../controllers/process_sort.php?sort=asc">Price(Lowest to Highest</a>
						</li>
						<li class="list-group-item">
							<a href="../controllers/process_sort.php?sort=desc">Price(Highest to Lowest</a>
						</li>
					</ul>
				</div>
				<!-- item list -->
				<div class="col-lg-10">
					<div class="row">
						<?php 
						// review this later
						// session_start();

			// steps for retrieving items
			//1. create a query
			//2. Use mysqli_query to get the results
			//3. If array (use foreach)
			//4. If object (use mysqli_fetch_assoc to convert the date to an associative array)
			//4.1 Use foreach ($result as $key => $value) 

			$items_query= "SELECT * FROM items";
			// for filtering
			if(isset($_GET['category_id'])){

				$catId = $_GET['category_id'];
				$items_query .= " WHERE category_id = $catId";
			}
			// sort
			if(isset($_SESSION['sort'])){
				$items_query .= $_SESSION['sort'];
			}


			$items = mysqli_query($conn, $items_query);

			foreach ($items as $indiv_item){
			?>
			<div class="col-lg-4 py-2">
				<div class="card">
					<img class="card-img-top" height="300px" src="<?php echo $indiv_item['image'] ?>" alt="image">
					<div class="card-body">
						<h4 class="card-title"><?php echo $indiv_item['name']?></h4>
						<p class="card-text">Php <?php echo $indiv_item['price']?>.00</p>
						<p class="card-text"><?php echo $indiv_item['description']?></p>
						<?php 

							$catId = $indiv_item ['category_id'];
							$category_query = "SELECT * FROM categories WHERE id = $catId";

							$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));

							?>
								<p class="card-text">Category: <?php echo $category['name'] ?></p>			
					</div>
					<?php
                                            if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 1){
                                        ?>
                                            <div class="card-footer text-center">
                                                <a href="edit_item_form.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-secondary">Edit Item</a>
                                                <a href="../controllers/process_delete_item.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-danger">Delete Item</a>
                                            </div>
                                        <?php
                                            } else {
                                                ?>
                                            <div class="card-footer">
                                                <input type="number" class="form-control" value="1">
                                                <button class="btn btn-success addToCartBtn" data-id="<?php echo $indiv_item['id'] ?>">Add To Cart</button>
                                            </div>
                                                
                                                <?php
                                            }
                                        ?>
				</div>
			</div>

					<?php
					}
				?>
			</div>
					
		</div>
		
	</div>
</div>
<script type="text/javascript" src="../assets/scripts/addtocart.js"></script>
<?php
}

?>


<!-- <form action="../controllers/process_update_cart.php" method="POST">
							<input type="number" class="form-control" value="1" name="quantity">
							<input type="hidden" name="id" value="<?php echo $indiv_item['id']?>">
							<button type="submit" class="btn btn-primary">Add To Cart</button>
						</form> -->
<?php 
	require "../templates/template.php";

	function get_content(){
	require "../controllers/connection.php";
?>
	<h1 class="text-center py-5">ADD ITEM FORM</h1>

	<div class="container">
		<div class="col-lg-6 offset-lg-3">
			<form action="../controllers/process_add_item.php" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">Item Name: </label>
					<input type="text" name="name" class="form-control" placeholder="Item Name">
				</div>
				<div class="form-group">
					<label for="name">Item Price: </label>
					<input type="number" name="price" class="form-control" placeholder="Item Price">
				</div>
				<div class="form-group">
					<label for="name">Item Description: </label>
					<textarea name="description" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label for="name">Item Image: </label>
					<input type="file" name="image" class="form-control">
				</div>
				<div class="form-group">
					<label for="category_id">Item Category: </label>
					<select name="category_id" class="form-control">
						<?php 
							$category_query = "SELECT * FROM categories";

							$categories = mysqli_query($conn, $category_query);

							foreach ($categories as $indiv_category){
							?>
								<option value="<?php echo $indiv_category['id']?>"><?php echo $indiv_category['name'] ?></option>
							<?php
							}
						 ?>
					</select>
				</div>
				<button type="submit" class="btn btn-info">Add Item</button>
			</form>

		</div>
	</div>
<?php	
	}

 ?>
<?php 
	require "../templates/template.php";
	function get_content(){
?>
	<h1 class="text-center py-5">CART</h1>
	<hr>
	<div class="table-responsive">
		<table class="table table-striped table-borderd">
			<thead>
				<tr class="text-center">
					<th>Item</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					// check whethter we have $_SESSION CART
					// get the session cart, and display each in a <tr>
					// a. check the data type of our $_SESSION 
					// b. do the loop (foreach($data as $key => $value))
					// c. the goal is to get the id and the quantity.
					// d. Get the item details through Item_query
					// e. Select * from items WHERE id = the id we got from the foreach.
					// f. use mysqli_query to get individual item.
					// get the subtotal by multiplying indiv_item['price'] and quantity we got from the foreach.
					// get the total (in order for us to do so, we must initialize $total at the very top)
					// display the details in <tr><td>
					session_start();
					require '../controllers/connection.php';
					
					$total = 0;

					if (isset($_SESSION['cart'])) {
						foreach ($_SESSION['cart'] as $item_id => $item_quantity) {
							$item_query = "SELECT * FROM items WHERE id = $item_id";
							$indiv_item = mysqli_fetch_assoc(mysqli_query($conn, $item_query)); 

							$subtotal = $item_quantity * $indiv_item['price'];
							$total += $subtotal;
								?>
									<tr class="text-center">
										<td><?php echo $indiv_item['name']; ?></td>
										<td>Php <?php echo $indiv_item['price']; ?></td>
										<td>
											<form action="../controllers/process_update_cart.php" method="POST">
												<input type="hidden" name="fromCartPage" value="true">
												<input type="hidden" name="id" value="<?php echo $item_id?>">
												<input type="number" name="quantity" value="<?php echo $item_quantity ?>" class="form-control quantityInput">
											</form>
										</td>
										
										<td>Php <?php echo $subtotal; ?>.00</td>
										<td><a href="../controllers/process_remove_items.php?id=<?php echo $indiv_item['id']?>" class="btn btn-info">Remove item</a></td>
									</tr>

								<?php
						}
					}
				 ?>
				 <tr class="text-center">
				 	<td><a href="../controllers/process_empty_cart.php" class= "btn btn-primary">Empty Cart</a></td>
				 	<td></td>
				 	<td></td>
				 	<td>Total: Php <?php echo $total ?></td>
				 	<td></td>
				 </tr>
				 <tr class="text-center">
				 	<td>
				 		<form action="../controllers/process_checkout.php" method="POST">
				 			<button class="btn btn-info">Pay via COD</button>
				 		</form>
				 	</td>
				 	<td></td>
				 	<td></td>
				 	<td></td>
				 	<td></td>
				 </tr>
			</tbody>
		</table>
	</div>
	<script type="text/javascript" src="../assets/scripts/updatecart.js"></script>
<?php 		
}

 ?>
<!DOCTYPE html>
<html>
<head>
	<title>PHP E-commerce</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cyborg/bootstrap.css">
	<link href="https://fonts.googleapis.com/css?family=Fredoka+One&display=swap" rel="stylesheet">

	<style>
		body { background: #130c19 !important;}

		.card { background-color: #ff000000 !important;}
		.navbar.bg-dark { background-color: #80218821 !important; }
		h1, .h1 {
    		font-size: 4rem !important;
    		font-family: 'Fredoka One', cursive !important;
			}
		h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
    	margin-bottom: 0.5rem;
    	font-weight: 500;
    	line-height: 1.2;
    	color: #f1589194;
			}

	</style>

</head>
<body>
	
	<?php require "navbar.php";
		get_content();
		require "footer.php";

	?>
	

</body>
</html>
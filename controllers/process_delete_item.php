<!-- DELETE:

We need to send something (id) for the database to know which item to delete.
We'll send the id via url.
Create an anchor tag in the catalog - card footer together with the edit button and in the href include ?id=#input id here via php#
Create the delete controller. 
In the controller, capture the id via GET.
Write your delete query: important don't forget where. 
Use mysqli_query
Go back to where we came from. -->

<?php 
	require 'connection.php';
	$id = $_GET['id'];
	$delete_query = "DELETE FROM items WHERE id = $id";
	$result = mysqli_query($conn, $delete_query);
	header("Location: ".$_SERVER['HTTP_REFERER']);
 ?>